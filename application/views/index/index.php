<nav class="navbar navbar-expand-lg navbar-light bg-dark">
  <a class="navbar-brand text-light" href="home.html">Marco</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link text-light" href="<?= base_url(); ?>auth">Home <span class="sr-only"></span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-light" href="<?= base_url(); ?>auth/registration">Login</a>
      </li>
      <li class="nav-item ">
        <a class="nav-link text-light" href="<?= base_url(); ?>auth/login">Register</a>
      </li>
      <li class="nav-item ">
      </li>
    </ul>
  </div>
  <form class="form-inline">
    <input class="button form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
    <button class="btn btn-outline-light my-2 my-sm-0 ml-auto" type="submit">Search</button>
  </form>
</nav>  
<div class="row">
  <div class="card ml-auto" style="width: 12rem;">
  <img src="<?= base_url(); ?>assets/img/logo-m.jpg" class="card-img-top" >
  <div class="card-body">
    <h5 class="card-title">Cintaku</h5>
    <a href="#" class="btn btn-primary ">Rent</a>
    <a href="#" class="btn btn-primary ">Buy</a>
  </div>
</div>
<div class="card ml-auto" style="width: 12rem;">
  <img src="<?= base_url(); ?>assets/img/logo-m.jpg" class="card-img-top" >
  <div class="card-body">
    <h5 class="card-title">Cintaku</h5>
    <a href="#" class="btn btn-primary ">Rent</a>
    <a href="#" class="btn btn-primary ">Buy</a>
  </div>
</div>
<div class="card ml-auto" style="width: 12rem;">
  <img src="<?= base_url(); ?>assets/img/logo-m.jpg" class="card-img-top" >
  <div class="card-body">
    <h5 class="card-title">Cintaku</h5>
    <a href="#" class="btn btn-primary ">Rent</a>
    <a href="#" class="btn btn-primary ">Buy</a>
  </div>
</div>
<div class="card ml-auto" style="width: 12rem;">
  <img src="<?= base_url(); ?>assets/img/logo-m.jpg" class="card-img-top" >
  <div class="card-body">
    <h5 class="card-title">Cintaku</h5>
    <a href="#" class="btn btn-primary ">Rent</a>
    <a href="#" class="btn btn-primary ">Buy</a>
  </div>
</div>
<div class="card ml-auto mr-auto" style="width: 12rem;">
  <img src="<?= base_url(); ?>assets/img/logo-m.jpg" class="card-img-top" >
  <div class="card-body">
    <h5 class="card-title">Cintaku</h5>
    <a href="#" class="btn btn-primary ">Rent</a>
    <a href="#" class="btn btn-primary ">Buy</a>
  </div>
</div>

</div>

  <footer class="container-fluid text-center bg-dark">
        <div class="row">
            <div class="col-md-4">
                <h5 class="text-monospace text-light mt-4">Social Media</h5>
                <ul class="list-unstyled text-small">
                    <li><a class="text-muted text-danger" href="https://forum.xda-developers.com/member.php?u=8244327" >Xda</a></li>
                    <li><a class="text-muted text-danger" href="https://markochristopher.blogspot.com/">Blog</a></li>
                    <li><a class="text-muted text-danger" href="https://www.facebook.com/marco.christopher1" >Facebook</a></li>
                </ul>
            </div>