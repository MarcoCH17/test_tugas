const card_1 = $('#Card-1');
const card_2 = $('#Card-2');
const card_3 = $('#Card-3');
const warning = $('small');
var dataLama;
var dataBaru;

function post() {
	const emailbox = $('#EmailInput');
	dataLama = emailbox.val();
	if (!emailChecker(dataLama)) {
		invalid(emailbox);
		warning.html('email is invalid');
	} else {
		valid(emailbox);
		warning.html('');
		showOld(dataLama);
		// passOld(dataLama);
		secondPage();
	}
}

function postAgain() {
	const emailbox = $('#NewEmailInput');
	dataBaru = emailbox.val();
	if (!alnumChecker(dataBaru)) {
		invalid(emailbox);
		warning.html('only a-z 0-9 is allowed');
	} else {
		if (confirm('Are you sure about your new email address?')) {
			valid(emailbox);
			warning.html('');
			var sensor = censor(dataBaru);
			showNew(sensor);
			thirdPage();
		}
	}
}

function firstPage() {
	card_2.hide();
	card_1.show();
}

function secondPage() {
	card_1.hide();
	card_2.show();
}

function thirdPage() {
	card_2.hide();
	card_3.show();
}

function emailChecker(a) {
  	const expression = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  	return expression.test(a);
}

function alnumChecker(a) {
  	const expression = /^[a-zA-Z0-9]+$/;
  	return expression.test(a);
}

function invalid(a) {
	a.addClass('email-invalid');
}

function valid(a) {
	a.removeClass('email-invalid');
}

function showOld(a) {
	$('#EmailOld').html(a);
}

function showNew(a) {
	$('#EmailNew').html('Your email address has been changed to ' + a + '@gmail.com');
}

// function passOld(a) {
// 	$('#Hidden').val(a);
// }

function censor(a) {
	str = Math.ceil(a.length/2);
	b = a.slice(str);
	for(i=0; i<str; i++) {
		c = '*' + b;
		b = c;
	}
	return b;
}