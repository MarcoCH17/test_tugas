var dataLama;
var dataBaru;

function submit() {
	const emailbox = document.getElementById('EmailInput');
	dataLama = emailbox.value;
	const warning = document.getElementById('Warning');
	if (!validateEmail(dataLama)) {
		invalidEmail(emailbox);
		warning.innerHTML = 'email is invalid';
	} else {
		validEmail(emailbox);
		warning.innerHTML = '';
		showOldEmail(dataLama);
		passOldEmail(dataLama);
		secondPage();
	}
}

function submitAgain() {
	const emailbox = document.getElementById('NewEmailInput');
	dataBaru = emailbox.value;
	const warning = document.getElementById('Warning2');
	if (confirm('Are you sure about your new email address?')) {
		if (!validateSecondEmail(dataBaru)) {
			invalidEmail(emailbox);
			warning.innerHTML = 'only a-z 0-9 is allowed';
		} else {
			validEmail(emailbox);
			warning.innerHTML = '';
			var sensor = censor(dataBaru);
			showNewEmail(sensor);
			thirdPage();
		}
	}
}

const card_1 = document.getElementById('Card-1');
const card_2 = document.getElementById('Card-2');
const card_3 = document.getElementById('Card-3');

function firstPage() {
	card_2.classList.add('d-none');
	card_1.classList.remove('d-none');
}

function secondPage() {
	card_1.classList.add('d-none');
	card_2.classList.remove('d-none');
	document.getElementById('Form-2').reset();
}

function thirdPage() {
	card_2.classList.add('d-none');
	card_3.classList.remove('d-none');
}

function validateEmail(a) {
  	const expression = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  	return expression.test(a);
}

function validateSecondEmail(a) {
  	const expression = /^[a-z0-9]+$/;
  	return expression.test(a);
}

function invalidEmail(a) {
	a.classList.add('email-invalid');
}

function validEmail(a) {
	a.classList.remove('email-invalid');
}

function showOldEmail(a) {
	document.getElementById('EmailOld').textContent = a;
}

function showNewEmail(a) {
	document.getElementById('EmailNew').textContent = 'Your email address has been changed to ' + a + '@gmail.com';
}

function passOldEmail(a) {
	document.getElementById('OldEmailInput').setAttribute('value', a);
}

function censor(a) {
	str = Math.ceil(a.length/2);
	b = a.slice(str);
	for(i=0; i<str; i++) {
		c = '*' + b;
		b = c;
	}
	return b;	
}