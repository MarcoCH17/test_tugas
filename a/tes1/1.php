<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Dashboard</title>

  <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="datetimepicker/jquery.datetimepicker.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <!-- Custom fonts for this template -->
  <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:500,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Muli:400,400i,800,800i" rel="stylesheet">
  <link href="assets/fontawesome-free/css/all.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
    <a class="navbar-brand js-scroll-trigger" href="#page-top">
      <span class="d-block d-lg-none">Welcome</span>
      <span class="d-none d-lg-block">
        <img class="img-fluid img-profile rounded-circle mx-auto mb-2" src="img/profile.jpg" alt="">
      </span>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger">
            <?php
          class input{
            public $id;
            public $nama;

            public function setid($id){
              $this->id = $id;
            }
            public function setnama($nama){
              $this->nama = $nama;
            }
            public function getid(){
              return $this->id;
            }
            public function getnama(){
              return $this->nama;
            }
            }
            $a = new input;
            $a->setid($_POST['username']);
            
            echo "Nama :".$a->getid()."<br>";
          ?>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#home">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#about">About</a>
        </li>
      </ul>
    </div>
  </nav>

  <div class="container-fluid p-0">

    <section class="p-3 p-lg-5 d-flex align-items-center" id="home">
     <div class="container my-5">
      <form method="POST" action="index.html" class="col-md-14">
      <div class="card-deck">
        <div class="card">
           <div class="card-body">
            <h4 class="card-title">Jenis Mobil</h4>
            <select id="opsi" name="option">
        <option value="Avanza">Avanza B 8978 BB</option>
        <option value="Xenia">Xenia B 2898 OI</option>
        <option value="Ertiga">Ertiga B 1876 KLM</option>
      </select>
          <div class="card-body">
            <h4 class="card-title">Tanggal & Waktu Peminjaman</h4>
            <input type="text" id="datetimepicker1">
          </div>
          <div class="card-footer bg-primary">
            <small class="text-white">Pilih Tanggal & Waktu</small>
          </div>
          <div class="card-body">
            <h4 class="card-title">Tanggal & Waktu Peminjaman</h4>
            <input type="text" id="datetimepicker2">
          </div>
          <div class="card-footer bg-primary">
            <small class="text-white">Pilih Tanggal & Waktu</small>
          </div>
           <div class="card-body">
            <h4 class="card-title">Tujuan</h4>
            <textarea class="form-control" rows="5" id="tuj"></textarea>
          </div>
        </div>
      </div> 
     </div>
    <input type="submit" name="submit" value="Simpan" class="btn btn-primary col-md-12 my-4">    
  </form>
    </section>

    <hr class="m-0">

    <section class=" p-3 p-lg-5 d-flex justify-content-center" id="about">
      

    </section>

    <hr class="m-0">

  </div>
  
  <script src="assets/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="assets/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>
