<?php
class Crud_model extends CI_Model{
  //get all categories method
  function get_user_role(){
      $result=$this->db->get('user_role');
      return $result;
  }
  //generate dataTable serverside method
  function get_all_user() {
      $this->datatables->select('name,email,active,id_role,role');
      $this->datatables->from('user');
      $this->datatables->join('user_role', 'role_id=id_role');
      $this->datatables->join('user_active', 'is_active=id_active');
      $this->datatables->add_column('view', '<a href="javascript:void(0);" class="edit_record btn btn-info" data-name="$1" data-email="$2" data-password="$3" data-category="$4">Edit</a>  <a href="javascript:void(0);" class="delete_record btn btn-danger" data-name="$1">Delete</a>','name,email,is_active,id_role,role');
      return $this->datatables->generate();
  }
  //insert data method
  function insert_user(){
      $data=array(
      'name' => htmlspecialchars($this->input->post('name',true)),
			'email' => htmlspecialchars($this->input->post('email',true)),
			'image' => 'default.jpg',
			'password' => password_hash($this->input->post('password1'),PASSWORD_DEFAULT),
			'is_active' => 1,
			'date_created' => time(),
      'role_id' => $this->input->post('category')
      );
      $result=$this->db->insert('user', $data);
      return $result;
  }
  //update data method
  function update_user(){
      $name=$this->input->post('name');
      $data=array(
        'name'         => $this->input->post('name'),
        'email'  => $this->input->post('email'),
        'password' => password_hash($this->input->post('password1'),PASSWORD_DEFAULT),
        'role_id'  => $this->input->post('category')
      );
      $this->db->where('name',$name);
      $result=$this->db->update('user', $data);
      return $result;
  }
  //delete data method
  function delete_user(){
      $name=$this->input->post('name');
      $this->db->where('name',$name);
      $result=$this->db->delete('user');
      return $result;
  }
}