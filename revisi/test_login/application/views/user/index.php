<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

    <div class="row">
        <div class="col-lg-8">
            <?= $this->session->flashdata('message'); ?>
        </div>
    </div>

    <div class="card mb-3" style="max-width: 780px;">
        <div class="row no-gutters">

            <div class="col-md-1 my-auto"> <br> </div>
            <div class="col-md-4 my-auto">
                <br>
                <img src="<?= base_url('assets/img/profile/') . $user['image']; ?>" class="card-img">
            </div>
            <div class="col-md-1 my-auto"></div>
            <div class="col-md-6 my-auto">
                <div class="card-body">
                    <h5 class="card-title"><?= $user['name']; ?></h5>
                    <p class="card-text"><?= $user['email']; ?></p>
                    <p class="card-text"><small class="text-muted">Member since <?= date('d F Y', $user['date_created']); ?></small></p>
                </div>

                <div class="table-responsive" style="max-width: 540px;">
                    <br>

                    <table class="table table-bordered table-striped">

                        <tr>
                            <td><b>IP Address</b></td>
                            <td><?php echo $ip_address; ?></td>
                        </tr>

                        <tr>
                            <td><b>Operating System</b></td>
                            <td><?php echo $os; ?></td>
                        </tr>

                        <tr>
                            <td><b>Browser Details</b></td>
                            <td><?php echo $browser . ' - ' . $browser_version; ?></td>
                        </tr>

                        <tr>
                            <td><b>QR Code</b></td>
                            <td><img src="<?php echo site_url('User/QRcode/' . $user['email']) ?>" alt=""></td>
                        </tr>


                    </table>
                </div>

            </div>


        </div>
    </div>
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->