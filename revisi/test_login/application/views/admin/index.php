<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

    <h2>User List</h2>
        <button class="btn btn-success pull-right" data-toggle="modal" data-target="#myModalAdd">Add New</button>
    <table class="table table-striped" id="mytable">
      <thead>
        <tr>
          <th>Nama</th>
          <th>Email</th>
          <th>Status</th>
          <th>Category</th>
          <th>action</th>
         
        </tr>
      </thead>
    </table>
</div>
<!-- /.container-fluid -->
<!-- Modal add -->
<div class="modal fade" id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="newSubmenuModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newSubmenuModalLabel">Add User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?php echo site_url('Admin/save');?>" method="post">
                <div class="modal-body">

                    <div class="form-group">
                        <input type="text" class="form-control" id="title" name="name" placeholder="Submenu Title">
                    </div>

                    <div class="form-group">
                        <select name="category" class="form-control" placeholder="Category" required>
                            <?php foreach ($category->result() as $row) :?>
                                 <option value="<?php echo $row->id_role;?>"><?php echo $row->role;?></option>
                            <?php endforeach;?>
                         </select>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" id="url" name="email" placeholder="Email">
                    </div>

                    <div class="form-group">
                        <input type="password" class="form-control" id="icon" name="password1" placeholder="Password">

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal update -->
<form id="add-row-form" action="<?php echo site_url('Admin/update');?>" method="post">
         <div class="modal fade" id="ModalUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       <h4 class="modal-title" id="myModalLabel">Update Product</h4>
                   </div>
                   <div class="modal-body">
                       <div class="form-group">
                           <input type="text" name="name" class="form-control" readonly >
                       </div>
                                         <div class="form-group">
                           <input type="text" name="email" class="form-control" placeholder="email" required>
                       </div>
                                         <div class="form-group">
                           <select name="category" class="form-control" required>
                                                     <?php foreach ($category->result() as $row) :?>
                                                         <option value="<?php echo $row->id_role;?>"><?php echo $row->role;?></option>
                                                     <?php endforeach;?>
                                                 </select>
                       </div>
                                         <div class="form-group">
                           <input type="password" name="password1" class="form-control" placeholder="Password" required>
                       </div>
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">Update</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>

     <!-- Modal delete -->
     <form id="add-row-form" action="<?php echo site_url('Admin/delete');?>" method="post">
         <div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       <h4 class="modal-title" id="myModalLabel">Delete Product</h4>
                   </div>
                   <div class="modal-body">
                           <input type="hidden" name="name" class="form-control" required>
                                                 <strong>Are you sure to delete this record?</strong>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                        <button type="submit" class="btn btn-success">Yes</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
 
</div>

<!-- End of Main Content -->