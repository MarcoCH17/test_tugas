<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url', 'form'));
        $this->load->library(array('form_validation', 'Recaptcha'));
    }

    public function index()
    {
        if ($this->session->userdata('email')) {
            redirect('user');
        }

        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        if ($this->form_validation->run() == false) {
            $data['title'] = 'User Login';
            $data = array(
                'captcha' => $this->recaptcha->getWidget(),
                'script_captcha' => $this->recaptcha->getScriptTag(),
            );
            $this->load->view('templates/auth_header', $data);
            $this->load->view('auth/login');
            $this->load->view('templates/auth_footer');
        } else {
            //validation success
            $this->_login();
        }
    }

    private function _login()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $recaptcha = $this->input->post('g-recaptcha-response');
        $user = $this->db->get_where('user', ['email' => $email])->row_array();
        $response = $this->recaptcha->verifyResponse($recaptcha);

        if ($response['success']) {
            //user registered
            if ($user) {
                //user active 
                if ($user['is_active'] == 1) {
                    //check password
                    if (password_verify($password, $user['password'])) {
                        $data = [
                            'email' => $user['email'],
                            'role_id' => $user['role_id']
                        ];
                        $this->session->set_userdata($data);
                        if ($user['role_id'] == 1) {
                            redirect('admin');
                        } else {
                            redirect('user');
                        }
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                        Wrong password
                        </div>');
                        redirect('auth');
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                    This email is not actived
                    </div>');
                    redirect('auth');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                This email is not registered
                </div>');
                redirect('auth');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Please verify using Recaptcha
            </div>');
            redirect('auth');
        }
    }

    public function registration()
    {
        if ($this->session->userdata('email')) {
            redirect('user');
        }

        $this->form_validation->set_rules('name', 'Name', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[user.email]', [
            'is_unique' => 'this email has already been taken'
        ]);
        $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[6]|matches[password2]', [
            'matches' => 'password does not match',
            'min_length' => 'password too short'
        ]);
        $this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');

        if ($this->form_validation->run() == false) {
            $data['title'] = 'User Registration';
            $data = array(
                'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
                'script_captcha' => $this->recaptcha->getScriptTag(), // javascript recaptcha ditaruh di head
            );
            $this->load->view('templates/auth_header', $data);
            $this->load->view('auth/registration');
            $this->load->view('templates/auth_footer');
        } else {
            // $code = substr(str_shuffle($set), 0, 12);
            $email=$this->input->post('email', true);
            $data = [
                'name' => htmlspecialchars($this->input->post('name', true)),
                'email' => htmlspecialchars($email),
                'image' => 'default.png',
                'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
                'role_id' => 2,
                'is_active' => 0,
                'date_created' => time(),
                // 'code' => $code
            ];
            $token =base64_encode(random_bytes(32));
            $user_token = [
                'email' => $email,
                'token' => $token,
                'date_created' => time()
            ];

            $this->db->insert('user', $data);
            $this->db->insert('user_token', $user_token);
            $this->_send($this->input->post('email', true),$token);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                Congratulation! your account has been created. <br>please Verify
            </div>');
            redirect('auth');
        }
    }
    private function _send($to,$token){
        $this->load->library('phpmailer_lib');

        $mail = $this->phpmailer_lib->load();

        // SMTP configuration
        $mail->isSMTP();
        $mail->Host     = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'bobohomarco@gmail.com';
        $mail->Password = '1234sarman';
        $mail->SMTPSecure = 'tls';
        $mail->Port     = 587;

        $mail->setFrom('bobohomarco@gmail.com', 'Marco');


        $mail->addAddress($to);

        // Add cc or bcc 
        // $mail->addCC('cc@example.com');
        // $mail->addBCC('bcc@example.com');


        $mail->Subject = 'Account Verification';


        $mail->isHTML(true);


        $mailContent = 'Click This Link to verify : <a href="'. base_url() .'auth/verify?email=' . $to . '&token=' . urlencode($token). '">Aktivasi Akun</a>';
        $mail->Body = $mailContent;

        if(!$mail->send()){
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        }else{
        }
    }
    public function verify()
    {
        $mail = $this->input->get('email');
        $token = $this->input->get('token');

        $user = $this->db->get_where('user',['email' => $mail])->row_array();
        if($user) {
            $user_token = $this->db->get_where('user_token',['token' => $token])->row_array();
            if($user_token) {
               if(time() - $user_token['date_created'] <(60 * 60 * 12)){
                    $this->db->set('is_active',1);
                    $this->db->where('email',$mail);
                    $this->db->update('user');

                    $this->db->delete('user_token',['email' => $mail]);
                    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"> Verify Success </div>');
                    redirect('auth');  
               }else{

                $this->db->delete('user',['email' => $mail]);
                $this->db->delete('user_token',['email' => $mail]);
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Token Expired </div>');
                redirect('auth');  
               }
             } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Wrong Token </div>');
                redirect('auth');
            }
         } else{

            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> email not valid </div>');
            redirect('auth');
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('role_id');

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                You are logged out
            </div>');
        redirect('auth');
    }

    public function blocked()
    {
        $this->load->view('auth/blocked');
    }
}
